import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../product';

@Component({
  selector: 'app-related-products',
  templateUrl: './related-products.component.html',
  styleUrls: ['./related-products.component.scss']
})
export class RelatedProductsComponent implements OnInit {
  @Input('products') relatedProducts: Product[] = [];

  private allColorsClass: string = 'hide';

  constructor() { }

  ngOnInit() {
  }

  showAllColors() {
    this.allColorsClass = this.allColorsClass === 'hide' ? 'show' : 'hide';
  }

}
