import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DescriptionFeatruresComponent } from './description-featrures.component';

describe('DescriptionFeatruresComponent', () => {
  let component: DescriptionFeatruresComponent;
  let fixture: ComponentFixture<DescriptionFeatruresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DescriptionFeatruresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DescriptionFeatruresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
