import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-description-featrures',
  templateUrl: './description-featrures.component.html',
  styleUrls: ['./description-featrures.component.scss']
})
export class DescriptionFeatruresComponent implements OnInit {
  @Input() description: string;

  constructor() { }

  ngOnInit() {
  }

}
