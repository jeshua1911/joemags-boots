import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryDataService } from './in-memory-data.service';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { HomeComponent } from './home/home.component';
import { ProductViewComponent } from './product-view/product-view.component';
import { LayoutComponent } from './layout/layout.component';
import { ProductsComponent } from './products/products.component';
import { ProductListComponent } from './product-list/product-list.component';

import { ProductsService } from './products.service';
import { RelatedProductsComponent } from './product-view/related-products/related-products.component';
import { DescriptionFeatruresComponent } from './product-view/description-featrures/description-featrures.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductViewComponent,
    LayoutComponent,
    ProductsComponent,
    ProductListComponent,
    RelatedProductsComponent,
    DescriptionFeatruresComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [ ProductsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
