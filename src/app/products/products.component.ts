import { Component, OnInit } from '@angular/core';
import { STYLE_FILTERS, SHOE_SIZES, COLOR_FILTERS, FEATURES, PRICES } from './style-filters.module';
import { Product } from '../product';
import { Products } from '../list';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  filterToggle = 'down';
  filters = {
    'style' : {
      'collapsed'  : true,
      'six_inches' : false,
      'winter'     : false,
      'hiking'     : false,
      'chukka'     : false,
      'sneaker'    : false,
      'side_zip'   : false,
      'list_mode'  : 'More'
    },
    'shoe_size' : {
      'collapsed'  : true,
    },
    'width' : {
      'collapsed'  : true,
    },
    'color' : {
      'collapsed'  : true,
    },
    'features' : {
      'collapsed'  : true,
    },
    'price' : {
      'collapsed'  : true,
    },
  };

  styleFilters = STYLE_FILTERS;
  shoeSizes = SHOE_SIZES;
  colorFilters = COLOR_FILTERS;
  featureFilters = FEATURES;
  priceFilters = PRICES;
  showCount: number = 10;
  hasMoreProducts: boolean = true;
  products: Array<Product> = [];

  constructor(private productsService: ProductsService) {
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.productsService.getProducts()
      .subscribe(products => this.products = products);
  }

  onFilterToggle () {
    if(this.filterToggle === 'down') {
      this.filterToggle = 'up';
    } else {
      this.filterToggle = 'down';
    }
  }

  onFilterSectionToggle(section){
    this.filters[section].collapsed = !this.filters[section].collapsed;
  }

  toggleMore(section){
    if(this.filters[section].list_mode === 'More'){
      this.filters[section].list_mode = 'Less';
    } else {
      this.filters[section].list_mode = 'More';
    }
  }

  loadMore(){
    this.showCount += 10;

    if(this.showCount > Products.length){
      this.hasMoreProducts = false;
    }
  }

}
