export const STYLE_FILTERS = [
  {
    'id' : 'six_inches',
    'label' : '6" and above'
  },
  {
    'id' : 'winter',
    'label' : 'Winter'
  },
  {
    'id' : 'hiking',
    'label' : 'Hiking'
  },
  {
    'id' : 'chukka',
    'label' : 'Chukka'
  },
  {
    'id' : 'sneaker',
    'label' : 'Sneaker Boot'
  },
  {
    'id' : 'side_zip',
    'label' : 'Side-Zip Boots'
  },
  {
    'id' : 'mids',
    'label' : 'Mids'
  },
  {
    'id' : 'slip_ons',
    'label' : 'Slip-Ons'
  },
  {
    'id' : 'dress_shoes',
    'label' : 'Dress Shoes'
  },
  {
    'id' : 'oxfords',
    'label' : 'Oxfords'
  },
  {
    'id' : 'roll_tops',
    'label' : 'Roll-Tops'
  },
];

export const SHOE_SIZES = [
  '6.5',
  '7',
  '7.5',
  '8',
  '8.5',
  '9',
  '9.5',
  '10',
  '10.5',
  '11',
  '11.5',
  '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18'
];


export const COLOR_FILTERS = [
  {
    'label' : 'Brown/Tan',
    'value' : '#8b4513'
  },
  {
    'label' : 'Black',
    'value' : '#000'
  },
  {
    'label' : 'Wheat/Yellow',
    'value' : '#ffd700'
  },
  {
    'label' : 'Grey',
    'value' : 'GREY'
  },
  {
    'label' : 'Blue',
    'value' : 'BLUE'
  },
  {
    'label' : 'Green',
    'value' : 'GREEN'
  },
  {
    'label' : 'Red',
    'value' : 'RED'
  },
  {
    'label' : 'White',
    'value' : '#fff'
  },
  {
    'label' : 'Orange',
    'value' : 'ORANGE'
  },
  {
    'label' : 'Pink/Purple',
    'value' : '#d629d6'
  },
]

export const FEATURES = [
  'WaterProof',
  'Anti-Fatigue Technology',
  'Ortholite Insoles',
  'Sensorflex Comfort System',
  'Insulated',
  'Water-Resistant',
  'Aerocore Energy System',
  'Gore-Tex Membrane'
];

export const PRICES = [
  '$51.00 - $100.00',
  '$101.00 - $150.00',
  '$151.00 - $200.00',
  '> $200.00'
];
