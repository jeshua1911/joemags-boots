import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../product';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  @Input('products') productList: Array<Product> = [];
  @Input('count') showCount: number = 0;

  constructor() { }

  ngOnInit() {
  }

}
