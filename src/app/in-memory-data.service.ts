import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Products } from './list';

@Injectable()
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    return { 'products' : Products };
  }
}
